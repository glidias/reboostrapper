<!doctype html>
<html class="no-js">
<head>
    <?php include_once 'inc/head.php';?>
</head>
<body>

    <div class="wrap">

        <?php include_once 'inc/header.php';?>

        <main class="main">
            <h1><?php echo "Hello World!"; ?></h1>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-2">column</div>
                    <div class="col-sm-4 col-md-2">column</div>
                    <div class="col-sm-4 col-md-2">column</div>
                    <div class="col-sm-4 col-md-2">column</div>
                    <div class="col-sm-4 col-md-2">column</div>
                    <div class="col-sm-4 col-md-2">column</div>
                </div>
            </div>
        </main>

        <?php include_once 'inc/footer.php';?>

    </div>

    <div class="lb-btn-open">open</div>
    <div class="lb-btn-close">close</div>

    <?php include_once 'inc/scripts.php';?>
</body>
</html>
