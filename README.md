# Bootstrap Starter Kit
## Starter kit to be more productive. By [Boon](http://simboonlong.com/)

This is a bootstrap(v3.3.7) starter kit, recipe cooked with some ideas that are picked up overtime.

---

### Requirements
* Node NPM
* Gulp
* WAMP/MAMP

### Features
* Gulp as taskrunner. (Solely for handling tasks automation, and not js/css plugins/frameworks dependancies management)
* Gulp compile scss to css, css prefixing, lint js with jshint, js uglify + concat + images compression.
* PHP includes for better code organization, scalability and maintainability.
* SCSS setup is based on the ideas of [smacss](https://smacss.com/).
* JS setup with revealing module pattern.
* Font awesome as standalone, to avoid scss import encoding issues.

### Documentation
Available commands:

* `gulp` : Compile + prefix scss to main.css, concat+uglify js dependancies.
* `gulp watch` : Watch for scss and js changes. Linting for js.
* `gulp serve` : Spin up a server with live-reload + browsersync. Requires vhost setup.
* `gulp build` : minify images, css, js for production.

### Basic first time setup
1. Open terminal and cd to project folder.
2. Run `npm install` to download Node dependancies.
3. Run `gulp` once to compile all neccessary images, css, js.
4. Run `gulp watch` to watch for changes while developing.

### Optional
* Use yarn versioning by `yarn install` to download Node dependancies.
* Run `gulp serve` to watch for changes while developing with live-reload + browsersync.
* Run `gulp clean:images` to clear images folder.
* Run `gulp clean:css` to clear css folder.
* Run `gulp clean:js` to clear js folder.
