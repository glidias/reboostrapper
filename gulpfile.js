'use strict';

var PROXY = 'http://localhost/bootstrap-starter-kit/site/'; // local.demo.com
var ASSETS_SRC = '_src';
var ASSETS_DEST = 'site';


var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var newer = require('gulp-newer');
var combineMq = require('gulp-combine-mq');
var browserSync = require('browser-sync');
var del = require('del');



// ====================================================================


// Delete css js folders
gulp.task('clean:images', function () {
    return del([
        ''+ASSETS_DEST+'/images'
    ]);
});

gulp.task('clean:css', function () {
    return del([
        ''+ASSETS_DEST+'/css'
    ]);
});

gulp.task('clean:js', function () {
    return del([
        ''+ASSETS_DEST+'/js'
    ]);
});



// Move files
gulp.task('move-files', function(){
    return gulp.src([
            ''+ASSETS_SRC+'/.htaccess',
            ''+ASSETS_SRC+'/data/**/*.json',
            ''+ASSETS_SRC+'/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}',
            ''+ASSETS_SRC+'/favicon/**/*.{png,xml,ico,json}'
        ], { base: ASSETS_SRC })
        .pipe(gulp.dest( ASSETS_DEST ));
});


// Minify images task
gulp.task('imagemin', function () {
    return gulp.src([
        ''+ASSETS_SRC+'/images/**/*.{gif,jpg,png,svg}'
    ])
        .pipe(newer(''+ASSETS_DEST+'/images/'))
        .pipe(imagemin({
            optimizationLevel: 3, // default
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(''+ASSETS_DEST+'/images/'))
        .pipe(browserSync.stream());
});


// Minify images task (build)
gulp.task('min-imagemin', ['clean:images'], function () {
    return gulp.src([
        ''+ASSETS_SRC+'/images/**/*.{gif,jpg,png,svg}'
    ])
        .pipe(newer(''+ASSETS_DEST+'/images/'))
        .pipe(imagemin({
            optimizationLevel: 3, // default
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(''+ASSETS_DEST+'/images/'))
        .pipe(browserSync.stream());
});


// ====================================================================



// Prefix and compile scss (watch)
gulp.task('css', function() {
    return gulp.src([
        ''+ASSETS_SRC+'/scss/**/*.scss'
    ])
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 4 version'],
            cascade: false
        }))
        .pipe(combineMq({
            beautify: true
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(''+ASSETS_DEST+'/css/'))
        .pipe(browserSync.stream());
});

// min css
gulp.task('min-css', ['clean:css'], function() {
    return gulp.src([
        ''+ASSETS_SRC+'/scss/**/*.scss'
    ])
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 4 version'],
            cascade: false
        }))
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(gulp.dest(''+ASSETS_DEST+'/css/'));
});



// Uglify + concat + Minify js, in sequence order
gulp.task('js', ['jshint'], function() {
    return gulp.src([
        ''+ASSETS_SRC+'/scripts/plugins/**/*.js', // ignored by jshintignore
        ''+ASSETS_SRC+'/scripts/components/app.js', // global app defined first
        ''+ASSETS_SRC+'/scripts/components/**/*.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(''+ASSETS_DEST+'/js/'))
        .pipe(browserSync.stream());
});


// Concat and uglify js (build)
gulp.task('min-js', ['jshint', 'clean:js'], function() {
    return gulp.src([
        ''+ASSETS_SRC+'/scripts/plugins/**/*.js', // ignored by jshintignore
        ''+ASSETS_SRC+'/scripts/components/app.js', // global app defined first
        ''+ASSETS_SRC+'/scripts/components/**/*.js'
    ])
        .pipe(stripDebug())
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(''+ASSETS_DEST+'/js/'));
});


// Lint js scripts
gulp.task('jshint', function() {
    return gulp.src([
        ''+ASSETS_SRC+'/scripts/components/**/*.js'
    ])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'))
});


// Launch the Server
gulp.task('browser-sync', ['default'], function() {
    browserSync({
        // server: {
        //     baseDir: './'+ASSETS_DEST+'/'
        // }
        proxy: PROXY
    });
});




// build without sourcemaps + strip debug
gulp.task('build', ['min-js', 'min-css', 'min-imagemin']);



// Watch task
gulp.task('watch', function () {
    gulp.watch([
        ''+ASSETS_DEST+'/**/*.php'
    ], browserSync.reload );
    gulp.watch([
        ''+ASSETS_SRC+'/images/**/*.{gif,jpg,png,svg,ico}'
    ], ['imagemin']);
    gulp.watch([
        ''+ASSETS_SRC+'/scss/**/*.scss',
    ], ['css']);
    gulp.watch([
        ''+ASSETS_SRC+'/scripts/**/*.js'
    ],['js']);
});


// Serve
gulp.task('serve', ['browser-sync', 'watch']);



// Default task
gulp.task('default', ['css', 'js', 'move-files', 'imagemin'], function () {

});


