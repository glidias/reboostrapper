// ====================================================================
// app.js
// ====================================================================

// /* jshint multistr: true */
// /* jshint validthis: true */
// /* jshint loopfunc: true */
// /* jshint boss:true  */
// /* jshint bitwise: false */
// /* jshint multistr: true */

var App = {}; // single global

(function( window, document, undefined ){
    'use strict';


    // App init after document loaded.
    $(document).ready(function() {

        App.lightbox.init();
        console.log( App.utility.getViewport().w, 'app initialized');

    });

})( this, this.document );
