App.lightbox = (function() {

    // settings
    var lbBtnOpen = $('.lb-btn-open');
    var lbBtnClose = $('.lb-btn-close');

    var deactivate = function() {

        console.log('close');

    };

    var activate = function() {

        console.log('open');

    };

    var init = function() {

        lbBtnOpen.on('click', activate );
        lbBtnClose.on('click', deactivate );

    };

    return {
        init : init
    };

})();




